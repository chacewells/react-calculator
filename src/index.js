import React from 'react';
import ReactDOM from 'react-dom';
import Operator from './operator';
import './index.css';

const DECIMAL = '.';

const DIVIDE = 0;
const MULTIPLY = 1;
const SUBTRACT = 2;
const ADD = 3;

class Calculator extends React.Component {
  operators = [
    Operator.divide(),
    Operator.multiply(),
    Operator.subtract(),
    Operator.add(),
  ];

  constructor(props) {
    super(props);
    this.state = {
      buffer: null,
      result: null,
      evalChain: [],
    };
  }

  calculate() {
    let buffer = this.state.buffer;
    let result = this.state.result;
    let evalChain = this.state.evalChain.slice();
    console.log('evalChain: ', evalChain);
    if (buffer != null)
      evalChain.push(Number(buffer));
    console.log('evalChain: ', evalChain);

    let i = 0;
    if (i < evalChain.length) {
      result = evalChain[i++];
      while (i < evalChain.length) {
        const operator = evalChain[i++];
        if (i >= evalChain.length)
            break;
        const other = evalChain[i++];
        result = operator.operation(result, other);
      }
    }

    return new Promise((resolve) => {
      this.setState({
        ...this.state,
        buffer: null,
        result,
        evalChain,
      }, resolve);
    });
  }

  appendOperator(i) {
    this.calculate().then(() => {
        const evalChain = this.state.evalChain.splice();
        this.setState({
          ...this.state,
          evalChain: (evalChain.length === 0 ? [this.state.result] : evalChain).concat([this.operators[i]]),
        });
    });
  }
  
  evaluate() {
    this.calculate().then(() => {
      this.setState({
        ...this.state,
        evalChain: []
      });
    });
  }

  clear() {
    this.setState({
      ...this.state,
      buffer: null,
      result: null,
      evalChain: [],
    });
  }

  appendToBuffer(char) {
    const curr = this.state.buffer || '';
    if (char === DECIMAL && curr.includes(char))
      return;
    const newBuffer = curr + String(char);
    this.setState({
      ...this.state,
      buffer: newBuffer,
    });
  }

  renderBuffer() {
    return <td colSpan="4">{this.state.buffer || this.state.result}</td>;
  }

  renderOperatorKey(i) {
    return <td><button type="button" onClick={() => this.appendOperator(i)}>{this.operators[i].label}</button></td>;
  }

  renderClearButton() {
    return <td colSpan="4"><button type="button" onClick={() => this.clear()}>Clear</button></td>;
  }

  renderEvaluateKey() {
    return <td><button type="button" onClick={() => this.evaluate()}> = </button></td>;
  }

  renderDecimal() {
    return this.renderInputKey(DECIMAL);
  }

  renderInputKey(char) {
    return <td><button type="button" onClick={() => this.appendToBuffer(char)}>{char}</button></td>;
  }

  render() {
    return (
      <table>
        <thead>
          <tr>
            {this.renderBuffer()}
          </tr>
        </thead>
        <tbody>
          <tr>
            {this.renderClearButton()}
          </tr>
          <tr>
            {this.renderInputKey('7')}
            {this.renderInputKey('8')}
            {this.renderInputKey('9')}
            {this.renderOperatorKey(DIVIDE)}
          </tr>
          <tr>
            {this.renderInputKey('4')}
            {this.renderInputKey('5')}
            {this.renderInputKey('6')}
            {this.renderOperatorKey(MULTIPLY)}
          </tr>
          <tr>
            {this.renderInputKey('1')}
            {this.renderInputKey('2')}
            {this.renderInputKey('3')}
            {this.renderOperatorKey(SUBTRACT)}
          </tr>
          <tr>
            {this.renderInputKey('0')}
            {this.renderDecimal()}
            {this.renderEvaluateKey()}
            {this.renderOperatorKey(ADD)}
          </tr>
        </tbody>
      </table>
    );
  }
}

ReactDOM.render(
  <Calculator />,
  document.getElementById('root')
);
