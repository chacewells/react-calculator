export default class Operator {
  label;
  operation;
  constructor(label, operation) {
    this.label = label;
    this.operation = operation;
  }
  static add() {
    return new Operator('+', (a, b) => a + b);
  }
  static subtract() {
    return new Operator('-', (a, b) => a - b);
  }
  static multiply() {
    return new Operator('×', (a, b) => a * b);
  }
  static divide() {
    return new Operator('÷', (a, b) => b !== 0 ? a / b : NaN);
  }
}

